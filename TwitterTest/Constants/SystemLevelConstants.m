//
//  SystemLevelConstants.m
//  TwitterTest
//
//  Created by Raghav Sai Cheedalla on 11/6/15.
//  Copyright © 2015 Ecovent. All rights reserved.
//

#import "SystemLevelConstants.h"


////////////// URLs ////////////////////
/* oAuth2 to get the token */
NSString * const kResource_URL          = @"oauth2/token";


///////////// GET URLs ////////////////
/* User Timeline url */
NSString * const kUserTimeline_URL      = @"1.1/statuses/user_timeline.json";
NSString *kAccessToken                  = @"";


@implementation SystemLevelConstants



@end
