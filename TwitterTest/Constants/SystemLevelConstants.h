//
//  SystemLevelConstants.h
//  TwitterTest
//
//  Created by Raghav Sai Cheedalla on 11/6/15.
//  Copyright © 2015 Ecovent. All rights reserved.
//

#import <Foundation/Foundation.h>


////////////// URLs ////////////////////
extern NSString * const kResource_URL;
extern NSString * const kUserTimeline_URL;

extern NSString *kAccessToken;

@interface SystemLevelConstants : NSObject

@end
