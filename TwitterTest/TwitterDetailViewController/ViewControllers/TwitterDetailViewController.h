//
//  TwitterDetailViewController.h
//  TwitterTest
//
//  Created by Raghav Sai Cheedalla on 11/7/15.
//  Copyright © 2015 Ecovent. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UserTimeLineModel;

@interface TwitterDetailViewController : UIViewController

@property (nonatomic, strong) UserTimeLineModel *userModel;


@end
